(ns plf03.core)


(defn función-comp-1
  []
  (let [f (fn [a] [(* 10 a) (* 3 a) (* 8 a)])
        g (fn [x] (sort x))
        z (comp  g f)]
    (z 5)))

(defn función-comp-2
  []
  (let [a (fn [x y] (+ (* 2 y) (* 3 x)))
        b (fn [x] (repeat 10 x))
        z (comp b a)]
    (z 5 6)))

(defn función-comp-3
  []
  (let [a (fn [a b c d] (+ a b c d))
        b (fn [x] (if (pos? x) (inc x) (dec x)))
        c (comp b a)]
    (c 1 2 3 4)))



(defn función-comp-4
  []
  (let [a (fn [x y] (if (> x y) [100 50 60] #{10 20 30}))
        b (fn [x] (sort x))
        d (comp  b a)]
    (d 5 1)))

(defn función-comp-5
  []
  (let [w (fn [x] (if (neg? x) (+ 2 x) (+ 5 x)))
        x (fn [x] (number? x))
        y (fn [x] (true? x))
        z (comp y x w)]
    (z 10)))

(defn función-comp-6
  []
  (let [a (fn [xs] (first xs))
        b (fn [x] (str x))
        c (comp b a)]
    (c [2020 11 17])))

(defn función-comp-7
  []
  (let [a (fn [xs] (count xs))
        b (fn [x] (str x))
        c (fn [x] (subs x 1))
        d (comp c b a)]
    (d [\p \r \o \g \r \a \m \a \c \i \o \n])))

(defn función-comp-8
  []
  (let [a (fn [as bs] (concat as bs))
        b (fn [xs] (map dec xs))
        c (fn [xs] (count xs))
        d (comp c b a)]
    (d [1 2 3 4 5 6 7 8 9 0] [10 20 30 40 50 60 70 80 90 100])))

(defn función-comp-9
  []
  (let [a (fn [x y z] (hash-set x y z))
        b (fn [x] (last x))
        c (comp b a)]
    (c 10 20 30)))

(defn función-comp-10
  []
  (let [a (fn [xs ys] (mapv (fn [x y] (+ x y)) xs ys))
        b (fn [xs] (map dec xs))
        c (comp b a)]
    (c [10 20 30] [40 50 60])))


(defn función-comp-11
  []
  (let [a (fn [x y] (== x y))
        b (fn [x] (true? x))
        c (comp b a)]
    (c 2 2)))

(defn función-comp-12
  []
  (let [a (fn [x y] (> x y))
        b (fn [x] (true? x))
        c (comp b a)]
    (c 1000 2)))

(defn función-comp-13
  []
  (let [a (fn [x y] (< x y))
        b (fn [x] (true? x))
        c (comp b a)]
    (c 1000 2)))

(defn función-comp-14
  []
  (let [a (fn [xs ys] (= (count xs) (count ys)))
        b (fn [x] (true? x))
        c (comp b a)]
    (c [1 2 3 4] [10 20 30 40])))

(defn función-comp-15
  []
  (let [a (fn [xs] (filter even? xs))
        b (fn [xs] (take 1 xs))
        c (fn [x] (mapv (fn [xs] (+ 1 xs)) x))
        d (comp c b a)]
    (d #{3 4 5 6 7})))

(defn función-comp-16
  []
  (let [a (fn [n xs] (take-while (fn [x] (== x n)) xs))
        b (fn [xs] (filter even? xs))
        c (fn [xs] (map dec xs))
        d (comp c b a)]
    (d 2 [2 2 2 2 10 20 30 5 10 10 20 20])))

(defn función-comp-17
  []
  (let [a (fn [xs ys zs] (concat xs ys zs))
        b (fn [xs] (map str xs))
        d (comp b a)]
    (d (vector \c \e \c \i \l \i \a) (vector \- \l \o \p \e \z) (vector \- \m \a \r \t \i \n \e \z))))

(defn función-comp-18
  []
  (let [a (fn [xs ys zs] (concat xs ys zs))
        b (fn [xs] (count xs))
        d (comp b a)]
    (d (vector \c \e \c \i \l \i \a) (vector \- \l \o \p \e \z) (vector \- \m \a \r \t \i \n \e \z))))

(defn función-comp-19
  []
  (let [a (fn [n xs] (take-last n xs))
        b (fn [xs] (map (fn [x] (+ 1 x)) xs))
        d (comp  b a)]
    (d 5 (hash-set 2 3 4 5 6 7 8 9 0))))

(defn función-comp-20
  []
  (let [a (fn [n xs] (sort-by n xs))
        b (fn [xs] (count xs))
        d (comp  b a)]
    (d :edad [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8} {:nombre "Ana" :edad 10} {:nombre "Rosa" :edad 12} {:nombre "Emma" :edad 2}])))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)



(defn función-complement-1
  []
  (let [f (fn [x] (associative? x))
        g (complement f)]
    (g [(list 1 2 3) (list 4 5 6)])))

(defn función-complement-2
  []
  (let [f (fn [x] (boolean? x))
        g (complement f)]
    (g true)))

(defn función-complement-3
  []
  (let [f (fn [x] (char? x))
        g (complement f)]
    (g (count #{1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18}))))

(defn función-complement-4
  []
  (let [f (fn [x] (char? x))
        g (complement f)]
    (g "a")))

(defn función-complement-5
  []
  (let [f (fn [x] (coll? x))
        g (complement f)]
    (g "a")))

(defn función-complement-6
  []
  (let [f (fn [x] (coll? x))
        g (complement f)]
    (g (list 10 20 30 40 50))))

(defn función-complement-7
  []
  (let [f (fn [x] (char? x))
        g (complement f)]
    (g \a)))

(defn función-complement-8
  []
  (let [f (fn [x] (decimal? x))
        g (complement f)]
    (g 1M)))

(defn función-complement-9
  []
  (let [f (fn [x] (double? x))
        g (complement f)]
    (g (last (map (fn [x] (* 2 x)) [10 20 30])))))

(defn función-complement-10
  []
  (let [f (fn [x] (float? x))
        g (complement f)]
    (g 0)))

(defn función-complement-11
  []
  (let [f (fn [x] (coll? x))
        g (complement f)]
    (g (first [(vector \c \e \c \i \l \i \a) (vector \- \l \o \p \e \z) (vector \- \m \a \r \t \i \n \e \z)]))))

(defn función-complement-12
  []
  (let [f (fn [x] (ident? x))
        g (complement f)]
    (g :valor)))

(defn función-complement-13
  []
  (let [f (fn [x] (indexed? x))
        g (complement f)]
    (g #{\a \b \c \d \e \f})))

(defn función-complement-14
  []
  (let [f (fn [x] (int? x))
        g (complement f)]
    (g 2020)))

(defn función-complement-15
  []
  (let [f (fn [x] (integer? x))
        g (complement f)]
    (g (+ 4 5))))

(defn función-complement-16
  []
  (let [f (fn [x] (keyword? x))
        g (complement f)]
    (g 'cecy)))

(defn función-complement-17
  []
  (let [f (fn [x] (list? x))
        g (complement f)]
    (g [1 2 3 4 5 6 7 8 {10 20 30 40} (list 50 60 70)])))

(defn función-complement-18
  []
  (let [f (fn [x] (map-entry? x))
        g (complement f)]
    (g (first {:a 1 :b 2}))))

(defn función-complement-19
  []
  (let [f (fn [x] (map? x))
        g (complement f)]
    (g (list 'a 'b 'c 'd 'e 'f 'g 'h))))

(defn función-complement-20
  []
  (let [f (fn [x] (pos-int? x))
        g (complement f)]
    (g (first (map (fn [x] (* 2 x)) [10 20 30])))))


(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)




(defn función-constantly-1
  []
  (let [a 1234
        f (constantly a)]
    (f \a 'a)))

(defn función-constantly-2
  []
  (let [a (first [20 30 40])
        f (constantly a)]
    (f [20 30 40])))

(defn función-constantly-3
  []
  (let [a (concat (list \c \e \c \i) (vector \- \l \o \p \e \z))
        f (constantly a)]
    (f 'a)))

(defn función-constantly-4
  []
  (let [a (conj (vector \c \e \c \i) (vector \- \l \o \p \e \z))
        f (constantly a)]
    (f true)))

(defn función-constantly-5
  []
  (let [a (distinct [1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 5 5 5])
        f (constantly a)]
    (f "cecy")))

(defn función-constantly-6
  []
  (let [a (drop-last [1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 5 5 5 6])
        f (constantly a)]
    (f)))

(defn función-constantly-7
  []
  (let [a (flatten '(1 2 [3 (4 5)]))
        f (constantly a)]
    (f [])))

(defn función-constantly-8
  []
  (let [a (max 20 30 40 50 10 100)
        f (constantly a)]
    (f (vector))))

(defn función-constantly-9
  []
  (let [a (min 20 30 40 50 10 100 1 2 3 4 5 6)
        f (constantly a)]
    (f (hash-set))))

(defn función-constantly-10
  []
  (let [a (map (fn [x] (* 100 x)) [10 20 30])
        f (constantly a)]
    (f (hash-map))))

(defn función-constantly-11
  []
  (let [a (count (map count [#{10 20} #{\a \b \c} #{true "hola" "cecy" 'a} []]))
        f (constantly a)]
    (f (+ 3 2))))

(defn función-constantly-12
  []
  (let [a (not-empty #{1 2 3 4 5 6 7 8})
        f (constantly a)]
    (f #{1 2 3 4 5})))

(defn función-constantly-13
  []
  (let [a (true? true)
        f (constantly a)]
    (f \a \b \b)))

(defn función-constantly-14
  []
  (let [a (odd? 2021)
        f (constantly a)]
    (f nil)))

(defn función-constantly-15
  []
  (let [a (nil? nil)
        f (constantly a)]
    (f boolean)))

(defn función-constantly-16
  []
  (let [a "17-11-2020"
        f (constantly a)]
    (f int)))

(defn función-constantly-17
  []
  (let [a (str 2020)
        f (constantly a)]
    (f 0.0)))

(defn función-constantly-18
  []
  (let [a (group-by :nombre [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8}  {:nombre "Ana" :edad 10} {:nombre "Rosa" :edad 12}  {:nombre "Emma" :edad 2}])
        f (constantly a)]
    (f 0.000001)))

(defn función-constantly-19
  []
  (let [a (sort-by :nombre [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8}  {:nombre "Ana" :edad 10}  {:nombre "Rosa" :edad 12} {:nombre "Emma" :edad 2}])
        f (constantly a)]
    (f [\a 'a [] #{} 3 [] (list)])))

(defn función-constantly-20
  []
  (let [a (sort-by :edad [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8} {:nombre "Ana" :edad 10}  {:nombre "Rosa" :edad 12}  {:nombre "Emma" :edad 2}])
        f (constantly a)]
    (f "hola")))


(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)



(defn función-every-pred-1
  []
  (let [a (fn [x] (number? x))
        b (fn [x] (odd? x))
        c (every-pred b a)]
    (c 3 9 11 13)))

(defn función-every-pred-2
  []
  (let [a (fn [x] (number? x))
        b (fn [x] (odd? x))
        c (every-pred a b)]
    (c 3 9 11 13)))

(defn función-every-pred-3
  []
  (let [a (fn [x] (coll? x))
        b (fn [x] (vector? x))
        c (every-pred b a)]
    (c [1 2 3 4] (vector 1 2 3 4))))

(defn función-every-pred-4
  []
  (let [a (fn [x] (indexed? x))
        b (fn [x] (coll? x))
        c (fn [x] (vector x))
        d (every-pred c b a)]
    (d [1 2 3 4] [] (list 10 20 30))))

(defn función-every-pred-5
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (keyword? x))
        c (every-pred b a)]
    (c :valor1 :valor2 10)))

(defn función-every-pred-6
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (number? x))
        c (fn [x] (int? x))
        d (every-pred c b a)]
    (d 1 2 3 4 5 6 7 8 9 10)))

(defn función-every-pred-7
  []
  (let [a (fn [x] (symbol? x))
        b (fn [x] (some? x))
        c (every-pred b a)]
    (c 'a 'b 'c 'd 'e 'f 'g)))

(defn función-every-pred-8
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (coll? x))
        c (fn [x] (map? x))
        d (every-pred c b a)]
    (d {:a 10 :b 20 :c 30 :d 40} (hash-map 1 2 3 4))))

(defn función-every-pred-9
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (number? x))
        c (fn [x] (pos-int? x))
        d (every-pred c b a)]
    (d 10 20 100.0 22 1M)))

(defn función-every-pred-10
  []
  (let [a (fn [x] (boolean? x))
        b (fn [x] (false? x))
        c (every-pred b a)]
    (c true)))

(defn función-every-pred-11
  []
  (let [a (fn [x] (some? (first x)))
        b (fn [x] (number? (first x)))
        c (every-pred b a)]
    (c [100 20 10 20 30 40 50 60 70])))
(defn función-every-pred-12
  []
  (let [a (fn [x] (some? (last x)))
        b (fn [x] (neg-int? (last x)))
        c (every-pred b a)]
    (c [100 20 10 20 -30 40 50 60 70])))

(defn función-every-pred-13
  []
  (let [a (fn [x] (associative? x))
        b (fn [x] (coll? x))
        c (every-pred b a)]
    (c [1 2 3 4 5 6 7 8] [9 10] [11 12] [13 15 16])))

(defn función-every-pred-14
  []
  (let [a (fn [x] (indexed? x))
        b (fn [x] (coll? x))
        c (every-pred b a)]
    (c [1 2 3 4 5 6 7 8] [9 10] [11 12] [13 15 16])))

(defn función-every-pred-15
  []
  (let [a (fn [x] (fn [x] (coll? x)) x)
        b (fn [x] (fn [x] (not-empty x)) x)
        c (every-pred b a)]
    (c [1 2 3 4 5 6 7 8] [9 10] [11 12] [13 15 16])))

(defn función-every-pred-16
  []
  (let [a (fn [x] (number? x))
        b (fn [x] (odd? x))
        c (every-pred b a)]
    (c 1 3 5 7 9)))

(defn función-every-pred-17
  []
  (let [a (fn [x] (coll? x))
        b (fn [x] (empty? x))
        c (every-pred b a)]
    (c [1 2 3 4 5 6 7 8] #{} [11 12] (list) [13 15 16])))

(defn función-every-pred-18
  []
  (let [a (fn [x] (neg? x))
        b (fn [x] (number? x))
        c (every-pred b a)]
    (c -1 -2 -3 -4 -5 -6)))

(defn función-every-pred-19
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (nil? x))
        d (every-pred b a)]
    (d nil)))

(defn función-every-pred-20
  []
  (let [a (fn [x] (pos? x))
        b (fn [x] (ratio? x))
        d (every-pred b a)]
    (d 0 2/3 -2/3 1/4 -1/10 5 3/3)))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [a (fn [x y z] (+ x y z)) 
        z (fnil a 10 20)]
    (z nil nil 30)))

(defn función-fnil-2
  []
  (let [a (fn [xs] (if (number? (first xs)) (map inc xs) (str "no es numero")))
        z (fnil a nil )]
    (z [10 20 30 40 50])))

(defn función-fnil-3
  []
  (let [a (fn [xs] (str "hola " xs))
        z (fnil a nil)]
    (z "cecy")))

(defn función-fnil-4
  []
  (let [a (fn [xs] (if (pos-int? (last xs)) (inc (first xs)) (dec (first xs))))
        z (fnil a nil)]
    (z [10 20 30 40 50])))

(defn función-fnil-5
  []
  (let [a (fn [xs ys] (== (count xs) (count ys)))
        z (fnil a [1 2 3 4 5 6 7])]
    (z nil [\c \e \c \i \l \i \a])))

(defn función-fnil-6
  []
  (let [a (fn [xs] (str "hola" xs))
        z (fnil a nil )]
    (z " Mundo clojure")))

(defn función-fnil-7
  []
  (let [a (fn  [xs x y] (str "hola " xs " Clojure" x " :)" y))
        z (fnil a nil)]
    (z " mundo" nil  nil)))

(defn función-fnil-8
  []
  (let [a (fn  [ f xs] (sort-by f xs))
        z (fnil a nil)]
    (z :edad [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8} {:nombre "ana" :edad 10} {:nombre "rosa" :edad 12} {:nombre "emma" :edad 2}])))

(defn función-fnil-9
  []
  (let [a (fn [s e] (range s e))
        z (fnil a nil)]
    (z 10 50)))

(defn función-fnil-10
  []
  (let [a (fn  [xs] (flatten xs))
        z (fnil a nil)]
    (z (list 2 3 4 (list 5 6 7)))))

(defn función-fnil-11
  []
  (let [a (fn [x y z] (- x y z)) 
        z (fnil a 10 )]
    (z nil 20 30)))

(defn función-fnil-12
  []
  (let [a (fn [xs] (if (number? (first xs)) (map inc xs) (str "1er element =! numero")))
        z (fnil a nil )]
    (z [\a 20 30 40 50])))

(defn función-fnil-13
    []
  (let [a (fn [x] (if (char? x) (repeat 10 x)(repeat 0 x)))
        z (fnil a nil)]
    (z \a)))

(defn función-fnil-14
    []
  (let [a (fn [xs] (if (pos-int? (last xs)) (inc (first xs)) (dec (first xs))))
        z (fnil a nil)]
    (z [10 20 30 40 -50])))

(defn función-fnil-15
  []
  (let [a (fn [xs ys] (< (count xs) (count ys)))
        z (fnil a [1 2 3 4 5 6 7 8 9 10] [\c \e \c \i \l \i \a])]
    (z nil nil)))

(defn función-fnil-16
  []
  (let [a (fn [xs n] (take-nth n xs))
        z (fnil a nil)]
    (z #{\a \b \c \d \e \f \g \h} 2)))

(defn función-fnil-17
  []
  (let [a (fn [x] (neg? x))
        z (fnil a nil)]
    (z 100)))

(defn función-fnil-18
  []
  (let [a (fn  [ f xs] (sort-by f xs))
        z (fnil a :nombre)]
    (z nil [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8} {:nombre "Ana" :edad 10} {:nombre "Rosa" :edad 12} {:nombre "Emma" :edad 2}])))

(defn función-fnil-19
  []
  (let [a (fn [i e s] (range i e s))
        z (fnil a nil)]
    (z 10 100 5)))

(defn función-fnil-20
  []
  (let [a (fn [xs] (indexed? xs))
        z (fnil a nil)]
    (z (list 1 2 \a \b \c \d))))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-juxt-1
  []
  (let [a (fn [s] (+ 2 s))
        b (fn [s] (+ 3 s))
        z (juxt b a)]
    (z 20)))

(defn función-juxt-2
  []
  (let [a (fn [as] (sort as))
        b (fn [xs] (first xs))
        z (juxt b a)]
    (z  [10 20 50 80 30 40])))

(defn función-juxt-3
  []
  (let [a (fn [as] (sort as))
        b (fn [xs] (first xs))
        z (juxt b a)]
    (z  [10 20 50 80 30 40])))

(defn función-juxt-4
  []
  (let [a (fn [a b c d] (+ a b c d))
        z (juxt a)]
    (z  10 20 50 80)))

(defn función-juxt-5
  []
  (let [a (fn [x] (if (pos? x) (inc x) (dec x)))
        z (juxt a)]
    (z  -100)))

(defn función-juxt-6
  []
  (let [a (fn [x] (if (pos? x) (inc x) (dec x)))
        z (juxt a)]
    (z  100)))

(defn función-juxt-7
  []
  (let [a (fn [n xs] (take n xs))
        z (juxt  a)]
    (z  5 (hash-set 1 10 2 20 3 30 4 40))))

(defn función-juxt-8
  []
  (let [a (fn [n] (inc n))
        b (fn [n] (inc n))
        c (fn [n] (inc n))
        z (juxt a b c)]
    (z 6)))
(función-juxt-8)
(defn función-juxt-9
  []
  (let [a (fn [n] (pos-int? n))
        b (fn [n] (decimal? n))
        z (juxt a b)]
    (z 10)))
(defn función-juxt-10
  []
  (let [a (fn [n] (dec n))
        b (fn [n] (inc n))
        c (fn [n] (* 2 n))
        z (juxt c b a)]
    (z 6)))



(defn función-juxt-11
  []
  (let [a (fn [xs] (* 2 (first xs)))
        b (fn [xs] (* 3 (first xs)))
        c (fn [xs] (* 4 (first xs)))
        z (juxt c b a)]
    (z [1 2 3 4 5 6 7 8])))

(defn función-juxt-12
  []
  (let [a (fn [xs] (inc  (last xs)))
        b (fn [xs] (dec  (last xs)))
        c (fn [xs] (pos? (last xs)))
        z (juxt c b a)]
    (z [-10 2 3 4 5 6 7 8])))

(defn función-juxt-13
  []
  (let [a (fn [xs] (map inc xs))
        b (fn [xs] (map (fn [x] (* 2 x)) xs))
        z (juxt b a)]
    (z [10 20 30 40])))

(defn función-juxt-14
  []
  (let [a (fn [xs] (map odd? xs))
        b (fn [xs] (map even? xs))
        c (fn [xs] (map some? xs))
        z (juxt c  b a)]
    (z (list 1 2 3 4 5 6 7 8))))

(defn función-juxt-15
  []
  (let [a (fn [xs] (reverse xs))
        b (fn [xs] (str xs))
        z (juxt  b a)]
    (z [\c \e \c \i \l \i \a])))

(defn función-juxt-16
  []
  (let [a (fn [xs] (sort-by :edad xs))
        b (fn [xs] (count xs))
        z (juxt a b)]
    (z [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8} {:nombre "ana" :edad 10} {:nombre "rosa" :edad 12} {:nombre "emma" :edad 2}])))

(defn función-juxt-17
  []
  (let [a (fn [x] (even? x))
        b (fn [x] (some? x))
        z (juxt b a)]
    (z 0)))

(defn función-juxt-18
  []
  (let [a (fn [x] (neg? x))
        b (fn [x] (nil? x))
        z (juxt b a)]
    (z -10)))

(defn función-juxt-19
  []
  (let [a (fn [x] (odd? x))
        b (fn [x] (number? x))
        z (juxt b a)]
    (z 2020)))

(defn función-juxt-20
  []
  (let [a (fn [x] (pos? x))
        b (fn [x] (int? x))
        z (juxt b a)]
    (z 20)))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)




(defn función-partial-1
  []
  (let [a (fn [x y z] (hash-set x y z))
        z (partial a 10 20)]
    (z 30)))

(defn función-partial-2
  []
  (let [a (fn [x] (inc x))
        z (partial a)]
    (z 5)))

(defn función-partial-3
  []
  (let [a (fn [xs] (map (fn [x] (* 2 x)) xs))
        z (partial a)]
    (z [10 20 30])))

(defn función-partial-4
  []
  (let [a (fn [x y z] (vector x y z))
        z (partial a 10 20)]
    (z 100)))

(defn función-partial-5
  []
  (let [a (fn [w x y z] (max w x y z))
        z (partial a 10 1 100)]
    (z 50)))

(defn función-partial-6
  []
  (let [a (fn [x] (dec x))
        z (partial a)]
    (z 11)))

(defn función-partial-7
  []
  (let [a (fn [xs] (map (fn [x] (+ 0 x)) xs))
        z (partial a)]
    (z [10 20 30])))

(defn función-partial-8
  []
  (let [a (fn [xs ys zs] (concat xs ys zs))
        z (partial a)]
    (z [10 20 30] [1 2 3] [0.1 0.2 0.3])))

(defn función-partial-9
  []
  (let [a (fn [xs] (map (fn [x] (pos? x)) xs))
        z (partial a)]
    (z (list -1 2 -3 4 -5 6))))

(defn función-partial-10
  []
  (let [a (fn [xs] (char? (first xs)))
        z (partial a)]
    (z (hash-set \a 'a \c 'c))))

(defn función-partial-11
  []
  (let [a (fn [a b c d] (+ a b c d))
        z (partial a 2 4)]
    (z 1 10)))

(defn función-partial-12
  []
  (let [a (fn [w x y z] (min w x y z))
        z (partial a 0.0 1 10)]
    (z 50)))
(defn función-partial-13
  []
  (let [a (fn [a b c d] (- a b c d))
        z (partial a 10 20)]
    (z 100 10)))

(defn función-partial-14
  []
  (let [a (fn [xs] (map (fn [x] (neg? x)) xs))
        z (partial a)]
    (z [10 20 30 40 50 60])))

(defn función-partial-15
  []
  (let [a (fn [a b c d] (* a b c d))
        z (partial a 10 20)]
    (z 100 10)))

(defn función-partial-16
  []
  (let [a (fn [n xs] (take-nth n xs))
        z (partial a)]
    (z 4 #{\a \b \c \d \e \f \g \h})))

(defn función-partial-17
  []
  (let [a (fn [xs] (map (fn [x] (even? x)) xs))
        z (partial a)]
    (z (list -1 2 -3 4 -5 6))))

(defn función-partial-18
  []
  (let [a (fn [n xs] (take-last n xs))
        z (partial a)]
    (z 4 (vector \a \b \c \d \e \f \g \h))))

(defn función-partial-19
  []
  (let [a (fn [n xs] (sort-by n xs))
        z (partial a)]
    (z :edad [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8} {:nombre "Ana" :edad 10}  {:nombre "Rosa" :edad 12} {:nombre "Emma" :edad 2}])))
(función-partial-19)
(defn función-partial-20
  []
  (let [a (fn [xs] (map (fn [x] (odd? x)) xs))
        z (partial a)]
    (z (vector 1 2 3 4 5 6 7 8))))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1
  []
  (let [a (fn [x] (map (fn [y] (neg? y)) x))
        b (fn [x] (map (fn [y] (odd? y)) x))
        c (some-fn b a)]
    (c [3 9 11 13])))

(defn función-some-fn-2
  []
  (let [a (fn [x] (number? x))
        b (fn [x] (odd? x))
        c (some-fn a b)]
    (c 3 9 11 13)))

(defn función-some-fn-3
  []
  (let [a (fn [x] (coll? x))
        b (fn [x] (vector? x))
        c (some-fn b a)]
    (c [1 2 3 4] (vector 1 2 3 4))))

(defn función-some-fn-4
  []
  (let [a (fn [x] (indexed? x))
        b (fn [x] (coll? x))
        c (fn [x] (vector x))
        d (some-fn c b a)]
    (d [1 2 3 4] [] (list 10 20 30))))

(defn función-some-fn-5
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (keyword? x))
        c (some-fn b a)]
    (c :valor1 :valor2 10)))

(defn función-some-fn-6
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (number? x))
        c (fn [x] (int? x))
        d (some-fn c b a)]
    (d 1 2 3 4 5 6 7 8 9 10)))

(defn función-some-fn-7
  []
  (let [a (fn [x] (symbol? x))
        b (fn [x] (some? x))
        c (some-fn b a)]
    (c 'a 'b 'c 'd 'e 'f 'g)))

(defn función-some-fn-8
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (coll? x))
        c (fn [x] (map? x))
        d (some-fn c b a)]
    (d {:a 10 :b 20 :c 30 :d 40} (hash-map 1 2 3 4))))

(defn función-some-fn-9
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (number? x))
        c (fn [x] (pos-int? x))
        d (some-fn c b a)]
    (d 10 20 100.0 22 1M)))

(defn función-some-fn-10
  []
  (let [a (fn [x] (boolean? x))
        b (fn [x] (false? x))
        c (some-fn b a)]
    (c true)))

(defn función-some-fn-11
  []
  (let [a (fn [x] (some? (first x)))
        b (fn [x] (number? (first x)))
        c (some-fn b a)]
    (c [100 20 10 20 30 40 50 60 70])))

(defn función-some-fn-12
  []
  (let [a (fn [x] (some? (last x)))
        b (fn [x] (neg-int? (last x)))
        c (some-fn b a)]
    (c [100 20 10 20 -30 40 50 60 70])))

(defn función-some-fn-13
  []
  (let [a (fn [x] (associative? x))
        b (fn [x] (coll? x))
        c (some-fn b a)]
    (c [1 2 3 4 5 6 7 8] [9 10] [11 12] [13 15 16])))

(defn función-some-fn-14
  []
  (let [a (fn [x] (indexed? x))
        b (fn [x] (coll? x))
        c (some-fn b a)]
    (c [1 2 3 4 5 6 7 8] [9 10] [11 12] [13 15 16])))

(defn función-some-fn-15
  []
  (let [a (fn [x] (fn [x] (coll? x)) x)
        b (fn [x] (fn [x] (not-empty x)) x)
        c (some-fn b a)]
    (c [1 2 3 4 5 6 7 8] [9 10] [11 12] [13 15 16])))

(defn función-some-fn-16
  []
  (let [a (fn [x] (number? x))
        b (fn [x] (odd? x))
        c (some-fn b a)]
    (c 1 3 5 7 9)))

(defn función-some-fn-17
  []
  (let [a (fn [x] (coll? x))
        b (fn [x] (empty? x))
        c (some-fn b a)]
    (c [1 2 3 4 5 6 7 8] #{} [11 12] (list) [13 15 16])))

(defn función-some-fn-18
  []
  (let [a (fn [x] (neg? x))
        b (fn [x] (number? x))
        c (some-fn b a)]
    (c -1 -2 -3 -4 -5 -6)))

(defn función-some-fn-19
  []
  (let [a (fn [x] (some? x))
        b (fn [x] (nil? x))
        d (some-fn b a)]
    (d nil)))

(defn función-some-fn-20
  []
  (let [a (fn [x] (pos? x))
        b (fn [x] (ratio? x))
        d (some-fn b a)]
    (d 0 2/3 -2/3 1/4 -1/10 5 3/3)))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)

